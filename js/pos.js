$(function()
{
  var temp_total=0;
  $(document).on("focus",".qtty",function()
  {
      temp_total=tag_total=Number($(this).parent("div").siblings("div").children(".tag-total").text());
      console.log(temp_total);
  }); 

  $(document).on("keyup mouseup",".qtty",function()
  { 
     var item_id=$(this).parent("div").siblings(".item_id_s").val(); 
     var price=Number($(this).parents("."+item_id).find(".tag-price").val());
     var quantity=Number($(".pl"+item_id).children(".quantity").val());
     var quantity_fixed=Number($(".pl"+item_id).children(".quantity_fixed").val());

     var qtty=Number($(this).parents('.'+item_id).find('.qtty').val()); 
     
     if(quantity_fixed<qtty)
     {
        $(this).parents('.'+item_id).find('.qtty').val(quantity_fixed);
        qtty = quantity_fixed;
     }

     if(quantity_fixed>=qtty)
     {
         var newtotal=qtty*price;
         $(this).parents("div").siblings("div").children(".tag-total").text(newtotal);
         $(".pl"+item_id).children(".quantity").val(quantity_fixed-qtty);
         $(".total_bill").val(Number($(".total_bill").val())-Number(temp_total-newtotal));
         temp_total=newtotal;

         if ($(".pl"+item_id).children(".quantity").val() == 0) {
           $(".pl"+item_id).parent(".box").fadeOut();
         } else {
           $(".pl"+item_id).parent(".box").fadeIn();
         }

     }
    $("."+item_id).children(".it_price").val(price);
    $("."+item_id).children(".it_qtty").val(qtty);
    $("."+item_id).children(".it_total").val(newtotal);
      disc();
  });

  // Adjust the price and recalculate associated calculations
  $(document).on("keyup mouseup", ".tag-price", function() {
    var item_id=$(this).parent("div").siblings(".item_id_s").val(); 
    var price=Number($(this).val());
    var qtty=Number($(this).parents('.'+item_id).find('.qtty').val());
    var total=Number($(this).parents('.'+item_id).find('.tag-total').text());
    var newtotal = qtty * price;

    $(this).parent("div").siblings("div").children(".tag-total").text(newtotal);
    $(".total_bill").val((Number($(".total_bill").val()) - total) + newtotal);
    disc();
  });

  $(document).on("click",".item_p",function()
  {
     var item_id=$(this).children(".item_id").val();
     console.log(item_id);
     var name=$(this).children(".name").val();
     var price=$(this).children(".price").val();
     var quantity=$(this).children(".quantity").val();
     var id_ex = document.getElementById("list_panel_pilihan_barang_"+item_id);
     if(id_ex!=null)
     {
         if(quantity!=0)
         {
            id_ex.classList.add(item_id);
            price=Number($("."+item_id).find(".tag-price").val());
            var qtty=$("."+item_id).find(".qtty").val();
            var total=Number($("."+item_id).find(".tag-total").text());
            qtty++;
            quantity--;
            var new_total=price*qtty;
            $(this).children(".quantity").val(quantity);
            $("."+item_id).find(".qtty").val(qtty); 
            $("."+item_id).find(".tag-total").text(new_total);
            $(".total_bill").val(Number($(".total_bill").val())+Number(price));
            $("."+item_id).children(".it_price").val(price);
            $("."+item_id).children(".it_qtty").val(qtty);
            $("."+item_id).children(".it_total").val(new_total);
            if (quantity==0) 
            {
              $(this).parent(".box").fadeOut();
            }
         }else
         {
              $(this).parent(".box").fadeOut();
         }
     }
     else
     {
        $(".detaild").append('\
          \
          <div id="list_panel_pilihan_barang_'+item_id+'" class="row_pur col-sm-12 '+item_id+'">\
            \
              <input type="hidden" name="prd_names[]" class="it_name" value="'+name+'"> \
              <input type="hidden" name="item_ids[]" class="it_id" value="'+item_id+'"> \
              <input type="hidden" name="prices[]" class="it_price" value="'+price+'"> \
              <input type="hidden" name="qtties[]" class="it_qtty" value="1"> \
              <input type="hidden" name="totals[]" class="it_total" value="'+price+'"> \
            \
                  <input type="hidden" class="item_id_s" value="'+item_id+'">\
                  <div class="r_i">\
                    <i  class="fa fa-times" aria-hidden="true"></i>\
                  </div>\
                  <div class="col-sm-3 cvv">\
                    <span class="tag-name">'+name+'</span>\
                  </div>\
                  <div class="col-sm-3 cvv">\
                    <input class="tag-price" type="number" value="'+price+'" min="0">\
                  </div>\
                  <div class="col-sm-3 cvv">\
                    <input class="qtty" type="number" value="1" min="0">\
                  </div>\
                  <div class="col-sm-3 cvv">\
                    <span class="tag-total">'+price+'</span>\
                  </div>\
            </div>');
        quantity--;
        $(this).children(".quantity").val(quantity);
        $(".total_bill").val(Number($(".total_bill").val())+Number(price));
     }
     disc();
  });

  $(document).on("click",".r_i",function()
  {
      var item_id=$(this).siblings(".item_id_s").val();
      var qtty=Number($("."+item_id).children("div").children(".qtty").val());
      var tag_total=Number($(this).siblings("div").children(".tag-total").text());
      var prd_qtty=Number($(".pl"+item_id).children(".quantity").val());
      console.log(".pl"+item_id);
      $(".pl"+item_id).children(".quantity").val(qtty+prd_qtty);
      var element = document.getElementById("list_panel_pilihan_barang_" +item_id);

      $(".pl"+item_id).parent(".box").fadeIn();
      var nt=Number($(".total_bill").val())-Number(tag_total);
      $(".total_bill").val(nt);
      element.parentNode.removeChild(element);
      disc();
  });
  $(document).on("change mouseup keyup",".disc",function()
  {
      disc();
  });

  function cal(){
    var item_name=[];
    var item_price=[];
    $('.tag-name').each(function(){
      item_name.push($(this).text());
    });

    $('.tag-price').each(function(){
      item_price.push($(this).text());
    });

    $.ajax({
      url: "insert.php",
      method: "POST",
      data: {item_name:item_name,item_price:item_price}, 
      success: function(){
         alert("insert");

          
          
      }
    });
  }

  /*
  $(document).on("click",".print",function()
  {
    var restorepage = $('body').html();
    var printcontent = $(".bill").clone();
    $('body').empty().html(printcontent);
    window.print();
  });



  $(document).on("click",".tx",function()
  {
    var checkBox=document.getElementById("tax_14");
    if (checkBox.checked == true){
      $(".tx").val(1);
      $(".af_tx").val(1);
    } else {
      $(".tx").val(0);
      $(".af_tx").val(0);
    }
    disc();
  });

  */
  function disc() {
    var ad;
    if (ad > 0) {
      ad=Number($(".total_bill").val())-Number($(".disc").val());
      console.log("+");
      $(".after_disc").val(ad);
      console.log("ad is" +ad);
    } else {
        ad=Number($(".total_bill").val())-(Number($(".disc").val()));
        $(".after_disc").val(-ad);
        console.log("ad is" +ad);

      }
      // var tax=Number($(".af_tx").val());
      /*if (tax==0) 
      {
         t=0;
         console.log("od1 "+t);
      }else
       {
         t=14;
         console.log("od2 "+t);
       }
       var after_tax=ad+(ad*t/100);*/

       //$(".after_tax").val(after_tax);
  }


    
  /*Hitung_Item(1);
  Hitung_Belanjaan(harga);
  Hitung_Discount();
  Hitung_Tagihan();*/
  //}



  /*function Hitung_Item(tambah)
  {
      jumlah_item += tambah;
      document.getElementById('hasil_item_list_belanja').innerHTML = "" + jumlah_item.toString() + " ";
      
  }
  function Hitung_Belanjaan(tambahan)
  {
      total_belanja += tambahan;
      document.getElementById('hasil_price_list_belanja').innerHTML = "Rp. " + total_belanja.toString() + ",-";
      
  }
  function Hitung_Tagihan()
  {
      var total;
          
      total = total_belanja - total_discount;
      document.getElementById('hasil_tagihan_list_belanja').innerHTML = "Rp. " + total.toString() + ",-";
      
  }
  function Hitung_Discount()
  {
      var percent_check;
      var disc;

      percent_check = discount.substr(discount.length - 1, 1);

      if (percent_check == "%")
      {
          disc = discount.substr(0, discount.length - 1);
          disc = parseInt(disc);
          if (isNaN(disc))
          {
              disc = 0;
          }
          total_discount = total_belanja * disc / 100;
      }
      else
      {
          total_discount = parseInt(discount);
          if (isNaN(total_discount))
          {
              total_discount = 0;
          }
      }

      document.getElementById('hasil_discount_list_belanja').innerHTML = "Rp. " + total_discount.toString() + ",-";
  }
  */
});