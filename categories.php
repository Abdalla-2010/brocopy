 <?php 
    session_start();
    if(isset($_SESSION['username'])){//  start of if check if is set session username camed or not
    	  $pagetitle="Categorise";
    	  include 'init.php';
 
          $do=isset($_GET['do'])? $_GET['do']:'Manage'; //check if do==what ?  ****************************
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

if($do=='Manage'){
    $sort='ASC';
    $sort_array=array('ASC','DESC');
    if(isset($_GET['sort']) && in_array($_GET['sort'], $sort_array) ){

      $sort=$_GET['sort'];

    }


     $stmt=$con->prepare("SELECT * FROM categories ORDER BY catID $sort");
     $stmt->execute();
     $cat=$stmt->fetchAll(); ?>

<h1 class="text-center"> Manage Categories</h1>
<div class="container   Categories">
  <div class="panel  panel-default">
	<div class="panel-heading">

  <i class="fa fa-edit"></i> Manage Categories
  <div class="option  pull-right">
  <i class="fa fa-sort"> </i> Ordering:[
      <a class="<?php if($sort == 'ASC'){ echo 'active';}?>" href="?sort=ASC">ASC</a> |
    <a class="<?php if($sort == 'DESC'){ echo 'active';}?>" href="?sort=DESC">DESC</a>]

     <i class="fa fa-eye"> </i>  View:[
    <span class="active"  data-view="full">Full</span>|
    <span data-view="classic">Classic</span>]
</div>
  </div>
    <div class="panel-body">
    	 <?php
           foreach ($cat as$ca) {
           echo '<div class="cat">';
           echo "<div class='hidden-button'>";
              echo "<a href='categories.php?do=Edit&catid=".$ca['catID'] ."' class='btn btn-xs btn-primary'>
              <i class='fa fa-edit'></i>Edit  </a>";
              echo "<a href='categories.php?do=Delete&catid=".$ca['catID'] ."'  class='  comfirm  btn btn-xs btn-danger'>
              <i class='fa fa-close'></i>Delete  </a>";


           echo "</div>";


           echo '<h3>'.$ca['catName'].'</h3>';

           echo '<div class="full-view">';
               echo "<p>";if ($ca['Descrption']==''){ echo 'This Categorie Has No Descrption';}
                else{ echo $ca['Descrption']; } echo '</p>';

          echo '</div>';
       echo '</div>';
       echo '<hr>';
           }


    	 ?>


    </div>
</div>

<a href="categories.php?do=Add" class=" addbtn btn btn-primary"><i class="fa fa-plus"> </i> Add New Category</a>
</div>

<?php
}


elseif($do=='Add'){?>


            <h1 class="text-center">Add Categories </h1>
            <div class="container">
              <form class="form-horizontal"  action="?do=Insert" method="POST">

                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Name</label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="namec" 
                      class="form-control" 
                      autocomplete="off" 
                      placeholder="Name Of Categorie" 
                      required="required"  />
                     </div>
                   </div>

                     <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Descrption</label>
                     <div class="col-sm-10 col-md-6">
                     <input 
                      type="text"
                      name="descrption"
                      class="form-control "
                      placeholder="Descrption Of Cat">
                     </div>
                   </div>

                     <div class="form-group">
                     <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="Add Categories " class="btn btn-primary  btn-lg" >
                     </div>
                   </div>

              </form>
            </div>

<?php
}

elseif($do == 'Insert'){
      if($_SERVER['REQUEST_METHOD']=='POST'){

           echo '<div class="container">';
           echo '<h1 class="text-center"> Insert Page</h1>';

           $cname		=$_POST['namec'];
           $cdescrption	=$_POST['descrption'];
        



        $check_Cat=checkItem('catName','categories',$cname);
       if($check_Cat==1){ 
                $MSA='<div class="alert alert-danger">  This Categorie is Exist befor</div>';
                Redurict($MSA,'');
      }else{

$stmt=$con->prepare("INSERT INTO categories(catName,Descrption)
                           VALUES( :cname, :cDescrption)
	                            ");
$stmt->execute(array(
                    'cname'=>$cname,
                    'cDescrption'=>$cdescrption
	));
    $mas='<div  class="alert alert-success"> '.$stmt->rowCount().' Inserted'.'</div>';
    Redurict($mas,'back');          
}

      }else{

        echo '<div class="container">';
        $mas='<div  class="alert alert-danger">You Not Allow To Come Here </div> ';
        Redurict($mas,'jjj');


      }


	echo '</div>';

}

///////////////////////////////////////////////////////////////////////////////////////////////


elseif($do=='Edit'){ 
 $catid=isset($_GET['catid']) && is_numeric($_GET['catid'])? intval($_GET['catid']):0;
           $stmt=$con->prepare("SELECT * 
                                FROM 
                                    categories
                                WHERE 
                                     catID=? 
                                   ");
           $stmt->execute(array($catid));
           $row=$stmt->fetch();

            $count=$stmt->rowCount();

            if($stmt->rowCount() > 0){  ?> 

<h1 class="text-center">Edit Categories </h1>
            <div class="container">
              <form class="form-horizontal"  action="?do=Update" method="POST">

                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Name</label>
                     <div class="col-sm-10  col-md-6">
                      <input type="hidden" name="catid" value="<?php echo $row['catID']; ?>" />

                      <input 
                      type="text" 
                      name="namec" 
                      class="form-control" 
                      placeholder="Name Of Categorie" 
                      required="required" 
                      value='<?php echo $row['catName']; ?>'
                       />
                     </div>
                   </div>

                     <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Descrption</label>
                     <div class="col-sm-10 col-md-6">
                     <input 
                      type="text"
                      name="descrption"
                      class="form-control "
                      placeholder="Descrption Of Cat"
                      value='<?php echo $row['Descrption']; ?>'
                      >
                     </div>
                   </div>




                     <div class="form-group">
                     <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="Save" class="btn btn-primary  btn-lg" >
                     </div>
                   </div>

              </form>
            </div>            
 <?php 
   /* if of end of edit page */
             }else{
              echo '<div class="container">';
              $mes='<div class="alert alert-danger">You are not alwoed to come here </div>';
              Redurict($mes);
              echo '</div>';

            }

}
//////////////////////////////////////////////////////////////////////////////////////////
         
elseif($do=='Update'){

  echo "<div class='container'>";
  echo '<h1 class="text-center">Update Category</h1>';


   if($_SERVER['REQUEST_METHOD']=='POST'){
      $catid         =$_POST['catid'];
      $catname       =$_POST['namec'];
      $catdescrption =$_POST['descrption'];
     
      $s=$con->prepare("UPDATE 
                             categories 
                           SET
                             catName=? , Descrption=? 
                           WHERE 
                              catID=? 
                                     ");

      $s->execute(array( $catname , $catdescrption,$catid ));

    $mas='<div class="alert alert-success">'.$s->rowCount() .' Record Updated </div>';
           Redurict($mas,'back');


    


   }else{


              echo '<div class="container">';
              $mas='<div class=" alert alert-danger"> YOU Not alowed to came here </div>';
              Redurict($mas,'back');
              echo '</div>';

   }


echo "</div>";
}

///////////////////////////////////////////////////////////////////////////////////////////////



elseif($do=='Delete'){ 

echo '<div class="container">';
echo '<h1 class="text-center"> Delete page </h1>';

$catid=isset($_GET['catid']) && is_numeric($_GET['catid'])? intval($_GET['catid']):0;

    $checkItem=checkItem('catID','categories',$catid);

          if($checkItem > 0){  
                  $stmt=$con->prepare("DELETE FROM categories WHERE catID=?");
                  $stmt->execute(array($catid));
                  if($stmt->rowCount() > 0){

                   $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Recored deleted'.'</div>';
                   Redurict($mas,'back');
                   }
          }

echo '</div>';

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
        include $tpl .'footer.php';
 //end of if check if is set session username camed or not****************************///////////////////////
    }else{
      
      echo 'You are not allawed to come here ';
    }