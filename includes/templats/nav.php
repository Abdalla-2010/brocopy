<nav class="navbar navbar-inverse">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-nav" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="dashbord.php"><?php echo "HOME-PAGE";   ?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="app-nav">
      <ul class="nav navbar-nav">

        <li><a href="items.php"><?php echo "Products"; ?></a></li>

        <li><a href="Reports.php"><?php echo "Reports"; ?></a></li>
        <li><a href="customers.php"><?php echo "Customers"; ?></a></li>
        <li><a href="machine.php"><?php echo "Machine"; ?></a></li>
        <li><a href="papers.php"><?php echo "Counters"; ?></a></li>
        
        <li><a href="archives.php"><?php echo "Archives"; ?></a></li>   
        <li><a href="notification.php"><?php echo "Notification"; ?></a></li>        
        <li><a href="visits.php"><?php echo "Visits"; ?></a></li> 
        <li><a href="debtes.php"><?php echo "Debt"; ?></a></li>    
                <li><a href="check.php"><?php echo "check "; ?></a></li>  
                                <li><a href="expenses.php"><?php echo "Expenses "; ?></a></li>    
                
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['name']; ?><span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li>
            <a href="editpropage.php?userid=<?php echo $_SESSION['ID'] ?>"> <?php echo "Edit_PRO"; ?></a>

            </li>
        <li><a href="Member.php?do=Manage"><?php echo "Members"; ?></a></li>
            <li><a href="logout.php"><?php echo "LOG_OUT"; ?></a></li>

          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>