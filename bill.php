<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>POS</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/pos.css">

    <script type="text/javascript" src="js/jquery-1.11.2.js"></script>
    <script type="text/javascript" src="js/n.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>



<body>
<?php 
include 'conect.php';
include 'functions/functions.php';
if (!isset($_POST['istax'])) {
    echo "ddd";
}
if (isset($_POST['total_before_disc'])&&isset($_POST['discount_amount'])&&isset($_POST['total_after_disc'])&&
isset($_POST['item_ids'])&&isset($_POST['prices'])&&
isset($_POST['qtties'])&&isset($_POST['totals'])&&isset($_POST['cust_name'])&&isset($_POST['istax'])&&isset($_POST['total_after_tax'])) {


    $total_before_disc=filter_var($_POST['total_before_disc'],FILTER_SANITIZE_STRING);
    $discount_amount=filter_var($_POST['discount_amount'],FILTER_SANITIZE_STRING);
    $total_after_disc=filter_var($_POST['total_after_disc'],FILTER_SANITIZE_STRING);
    $total_after_tax=filter_var($_POST['total_after_tax'],FILTER_SANITIZE_STRING);

    $item_ids=$_POST['item_ids'];
    $prices=$_POST['prices'];
    $qtties=$_POST['qtties'];
    $totals=$_POST['totals'];
    $istax=$_POST['istax'];
    if ($istax==0) {
        $t=0;
    }else
    {
        $t=14;
    }
    $prd_names=$_POST['prd_names'];
    $cust_name=filter_var($_POST['cust_name'],FILTER_SANITIZE_STRING);

    $func=new func();
    $bill_id=$func->add("bills","bill_name,total_b_desc,disc,total_a_desc,bill_num,tax14,total_a_tax,bill_date","?,?,?,?,?,?,?,now()","$cust_name,$total_before_disc,$discount_amount,$total_after_disc,0,$istax,$total_after_tax","yes");
    $bill_num="PO".$bill_id."S";
    $func->update("bills","bill_num=?","bill_id=?","$bill_num,$bill_id");
    ?>

    <div class="bill_con col-xs-12">
        <div class="col-xs-12" style="padding: 5px;"><a href="index.php"><div class="btn btn-primary">< Back</div></a></div>
        <div class="bill ">
            <div class="col-xs-12 cust_name">Customer Name: <?php echo $cust_name; ?></div>
            <div class="col-xs-12 cust_name">Bill Number: <?php echo $bill_num; ?></div>
            <div class="col-xs-12 products">Products:</div>
            <div class="tbl_products col-xs-12">
                <div class="col-xs-12 tbl_row_bill">
                    <div class="col-xs-3 td_bill">Name</div>
                    <div class="col-xs-3 td_bill">Quantity</div>
                    <div class="col-xs-3 td_bill">Price</div>
                    <div class="col-xs-3 td_bill">Total</div>
                </div>
                <div class="col-xs-12 tbl_body_bill">
                    <?php
                        for ($i=0; $i <count($prices); $i++) 
                        { 
                            $item_id=$item_ids[$i];
                            $price=$prices[$i];
                            $qttie=$qtties[$i];
                            $total=$totals[$i];
                            $prd_name=$prd_names[$i];//8655
?>
                <div class="col-xs-12 tbl_row_bill">
                    <div class="col-xs-3 td_bill_"><?php echo $prd_name; ?></div>
                    <div class="col-xs-3 td_bill_"><?php echo $qttie; ?></div>
                    <div class="col-xs-3 td_bill_"><?php echo $price; ?></div>
                    <div class="col-xs-3 td_bill_"><?php echo $total; ?></div>
                </div>
<?php   
                            $func->add("bill_items","bill_id,item_id,bi_qtty,bi_price,bi_total,item_name","?,?,?,?,?,?","$bill_id,$item_id,$qttie,$price,$total,$prd_name",null);
                            $func->update("items","pro_quantity=pro_quantity-?","Item_ID=?","$qttie,$item_id");

                        }                    
                    ?>
                </div>
                <div class="col-xs-12 tbl_row_bill vv">
                    <div class="col-xs-6 td_bill_">Sum</div>
                    <div class="col-xs-6 td_bill_"><?php echo $total_before_disc; ?></div>
                </div>
                <div class="col-xs-12 tbl_row_bill vv">
                    <div class="col-xs-6 td_bill_">Discount</div>
                    <div class="col-xs-6 td_bill_"><?php echo $discount_amount; ?> %</div>
                </div>
                <div class="col-xs-12 tbl_row_bill vv1">
                    <div class="col-xs-6 td_bill_">Total Bill</div>
                    <div class="col-xs-6 td_bill_"><?php echo $total_after_disc; ?></div>
                </div>
                <div class="col-xs-12 tbl_row_bill vv1">
                    <div class="col-xs-6 td_bill_">Tax Amount</div>
                    <div class="col-xs-6 td_bill_"><?php echo $t; ?> %</div>
                </div>
                <div class="col-xs-12 tbl_row_bill vv1">
                    <div class="col-xs-6 td_bill_">Total After Tax</div>
                    <div class="col-xs-6 td_bill_"><?php echo $total_after_tax; ?></div>
                </div>
                </div>
            </div>
            <div class="prnt">
                <button class="print">print</button>
            </div>
                
        </div>
    </div>

    <?php
    
}


?>


    <script src="js/plugins.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/pos.js"></script>
</body>

</html>