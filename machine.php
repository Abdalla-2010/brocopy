<?php 
  ob_start();
    session_start();
    if(isset($_SESSION['username'])){//  start of if check if is set session username camed or not
        $pagetitle="Customers";
        include 'init.php';
        ?>
 
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>


        <?php
          
        $do=isset($_GET['do'])? $_GET['do']:'Manage'; //check if do==what ?  ****************************
//////////////////////////////////////////////////////////////////////

//*****************start if of Manage Member Page***************//
        if($do=='Manage'){
        
        
        
        
        
         $stmt=$con->prepare("SELECT 
                              machin.* ,
                              users.name 
                           AS 
                           NAME 

                           
                           FROM 
                           machin
                           INNER JOIN  
                            users
                      ON users.ID=machin.cid

                           ");
        $stmt->execute(array($from,$to));
        $row=$stmt->fetchAll();

       
        
        


         ?>
            <h1 class="text-center">Manage machines </h1>

            <div class="container">

              <div class="table-responsive">
                  <a href="?do=Add"  class="btn  btn-primary"> <i class="fa  fa-plus "></i> Rental machine   </a>

              <a href="sale_machin.php?do=Add"  class="btn  btn-info"> <i class="fa  fa-plus "></i> Sale machine   </a>
               <a href="sale_machin.php"  class="btn  btn-info"> <i class="fa  fa-right "></i> sold machines   </a>

             <a href="customers.php?do=Add"  class="btn " style="background-color:#de915b;color: #fff;"> <i class="fa  fa-plus "></i> Add Customers   </a>

                  <table class=" min-table text-center table table-bordered ">
                     <tr>
                        <td>machine code</td>
                        <td>customerID </td>
                        <td>Date</td>
                        
                        <td>Model</td>
                        <td>Price</td>
                        <td>Defult Number</td>
                        
                        <td>Control</td>
                     </tr>




<?php   
foreach ($row as $k) {
  echo '<tr>';
      echo '<td>'.$k['mid'].'</td>';
      echo '<td>'. $k['NAME'].'</td>';
      
            echo '<td>'. $k['datead'].'</td>';
            
            echo '<td>'. $k['model'].'</td>';            
            echo '<td>'. $k['pap'].'</td>';            
            echo '<td>'. $k['max'].'</td>';              
      echo  "<td>
<a href='?do=Edit&userid=".$k['id']."'
class='btn btn-success'><i class='fa fa-edit'></i> Edit </a>


<a href='?do=Delete&id=".$k['id']."'
class='btn btn-danger  comfirm'><i class='fa fa-close'></i>Delete</a>

<a href='testpdf.php?id=".$k['id']."'
class='btn   '> <i class='fa fa-print'> </i >Print</a>

<a href='https://wa.me/".$k['phone']."?text=I am interested  in your car for sale'
class='btn btn-info' target='_blank' ><i class='fa fa-phone'></i>Send Message</a>

";


echo '</td>';

  echo '</tr>';
}

?>


                  </table>


              </div>
            
           </div>
       <?php  }//***********************************END if of Manage Member Page*********************//
////////////////////////////////////////////////////////////////////////////////////////////////////////// 





 //*************start if of Edit Member Page***********************// 
    elseif($do=='Add'){ /* if of start of edit page */
       ?> 
            
            <h1 class="text-center">Add  machine Information </h1>
            <div class="container">
              <form class="form-horizontal"  action="?do=insert" method="POST">
                      <input type="hidden" name="userid"  />

                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "serial number" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="code" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>


         <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "model" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="mod" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>
                   
                   

            <div class="form-group form-group-lg">
          <label class="col-sm-2  control-label">Type</label>
          <div class="col-sm-10  col-md-6">
          <select class="form-control" name="type">
<option value="0">Select type</option>
<option value='1'>b/w</option>
<option value='2'>Colors</option>
<option value='3'>Muf</option>
<option value='4'>PB/PC</option>

           </select>
          </div>
        </div> 
                  



                     <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Date</label>
                     <div class="col-sm-10 col-md-6">
                      <input type="date" name="date"
                       class="form-control"  required="required" >
                     </div>
                   </div>


            <div class="form-group form-group-lg">
          <label class="col-sm-2  control-label">Customer Name</label>
          <div class="col-sm-10  col-md-6">
          <select class="form-control" name="customer">
            <option value="0">....</option>
<?php
$stmt=$con->prepare("SELECT* FROM users");
$stmt->execute();
$cats=$stmt->fetchAll();
foreach ($cats as $cat) {
echo "<option value='".$cat["ID"]."'>".$cat["name"]."</option>";
}
?>
           </select>
          </div>
        </div> 
                                    



<div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "’Monthly Price" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="monthly_price" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>
                   
           <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "Price for copy" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="price_copy" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>
                   
                   
                   
                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "Minimum   copies" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="m_copy" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>
                   
                   



                
                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "contract Duration" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="Duration" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>


   <div class="form-group form-group-lg">
          <label class="col-sm-2  control-label">contract Type</label>
          <div class="col-sm-10  col-md-6">
          <select class="form-control" name="contract_type">
<option value="0">Select type</option>
<option value='1'>Monthly</option>
<option value='2'>Quarterly</option>

           </select>
          </div>
        </div> 
                  

                
                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "annual increase %" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="increase" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>




                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "Insurance" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="Insurance" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>


<div class="form-group form-group-lg">
 <div class="col-sm-10  col-md-6">

<label class="switch" style="margin-left: 194px;">
  <input type="checkbox" checked  name="button">
  <span class="slider round"></span>
</label>
</div>
</div>



                     <div class="form-group">
                     <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="Save" class="btn btn-primary  btn-lg" >
                     </div>
                   </div>

              </form>
            </div>

 <?php 
   /* if of end of edit page */
          

       }
//*******************************************END if of Edit Member Page******************************//
//////////////////////////////////////////////////////////////////////////////////////////////////////////




         
  elseif($do=='insert'){ /**********start if  of update page********************************************/
         echo '<h1 class="text-center">Insert Member </h1>';
         echo '<div class="container">';

         if( $_SERVER['REQUEST_METHOD']=='POST'){///start if of check if it post ****/

               $custmer = $_POST['customer'];
               $code=$_POST['code'];
               $date=$_POST['date'];

               $mod=$_POST['mod'];
               $type=$_POST['type'];
               $monthly_price=$_POST['monthly_price'];
               $price_copy=$_POST['price_copy'];               
               $m_copy=$_POST['m_copy'];               
               $Duration=$_POST['Duration'];    
               $contract_type=$_POST['contract_type'];               
               $increase=$_POST['increase'];               
               $Insurance=$_POST['Insurance'];               

if (isset($_POST['button'])) {

               $button='1';               
} else {

               $button='0';               
}
                   $formErrors=array();// var of array to have the error


                   if(strlen($cust) >20 ){ //  start if stmt only**//
                     $formErrors[]='<div class="alert alert-danger">usermname cant be <strong>>more than 20 char</strong></div>';
                  }//  end if stmt only**//

     
                  foreach ($formErrors as  $error) {
                    echo $error ;
                  }


          if(empty($formErrors)){ /**start if only to complet to  updet if no error*////

$stmt=$con->prepare(" INSERT INTO 
              machin(mid,cid,datead,pap,max,model,type,duration,increase,insurance,price_copy,contracr_type,button) 
            VALUES (:zmid , :zcid , :zdate,:zpap,:zmax,:zmodel ,:ztype,:zduration,:zincrease,:zinsurance,:zprice_copy,:zcontracr_type ,:zbutton)");

        $stmt->execute(array(

                          'zmid'=>$code,
                          'zcid'=>$custmer,
                          'zdate'=>$date,
                          'zpap'=>$monthly_price,
                          'zmax'=>$m_copy,
                          'zmodel'=>$mod,
                          'ztype'=>$type,
                          'zduration'=>$Duration,
                          'zincrease'=>$increase,
                          'zinsurance'=>$Insurance,
                          'zprice_copy'=>$price_copy,
                          'zcontracr_type'=>$contract_type,
                          'zbutton'=>$button         
                          
                           ));



           $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Record Inserted </div>';
           Redurict($mas,'back');

          }/********end  if only to complet to  updet if no error*/

            }///end if of check if it post ****/
            else{
              echo '<div class="container">';
              $mas='<div class=" alert alert-danger"> YOU Not alowed to came here </div>';
              Redurict($mas,'back');
              echo '</div>';
            }
echo '</div>';
   }/**********end if of updet page********************************/
////////////////////////////////////////////////////////////////////



       




 //*************start if of Edit Member Page***********************// 
    elseif($do=='Edit'){ /* if of start of edit page */
           //echo $_SESSION['ID'];
           $userid=isset($_GET['userid']) && is_numeric($_GET['userid'])? intval($_GET['userid']):0;
           $stmt=$con->prepare("SELECT * 
                                FROM 
                                    machin 
                                WHERE 
                                    id=? 
                                   
                                
                              LIMIT 1");
           $stmt->execute(array($userid));
           $row=$stmt->fetch();

            $count=$stmt->rowCount();

            if($stmt->rowCount() > 0){  ?> 
            
            <h1 class="text-center">Edi tmachine Information </h1>
            <div class="container">
              <form class="form-horizontal"  action="?do=Update" method="POST">
                      <input type="hidden" name="userid" value="<?php echo $userid ?>" />
                      
                      
                      
                      
                      
                      
                      
                
                   

                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "serial number" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="code" class="form-control" 
                      value="<?php echo $row['mid'] ?>" autocomplete="off"  required="required"  />
                     </div>
                   </div>



            <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "model" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="mod" class="form-control" 
                       autocomplete="off" value="<?php echo $row['model'] ?>"  required="required"  />
                     </div>
                   </div>

            <div class="form-group form-group-lg">
          <label class="col-sm-2  control-label">Type</label>
          <div class="col-sm-10  col-md-6">
          <select class="form-control" name="id">
<option value="0">Select type</option>
<option value='1'>b/w</option>
<option value='2'>Colors</option>
<option value='3'>Muf</option>
<option value='4'>PB/PC</option>

           </select>
          </div>
        </div> 
                     <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Date </label>
                     <div class="col-sm-10 col-md-6">
                      <input type="date" name="date"
                       value="<?php echo $row['datead'] ?>" class="form-control"  required="required" >
                     </div>
                   </div>








                  <div class=" form-group form-group-lg">
                    <label class="col-sm-2  control-label">Customer ID</label>
                     <div class="col-sm-10  col-md-6">
                      <select class="form-control" name="cid">
}
}
<?php
                      $stmt=$con->prepare("SELECT* FROM users");
                      $stmt->execute();
                      $subs=$stmt->fetchAll();
                     foreach ($subs as $sub){
                     echo "<option value='".$sub["ID"]."'";
                     if($row['cid'] == $sub["ID"]){echo 'Selected';}
                     echo ">".$sub["name"]."</option>";
                                     /*
                        echo "<option value='".$user["UserID"]."'>".$user["Username"]."</option>";   */
                                               }
?>
                      </select>
                    </div>
                  </div>
                    



                  <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "Price" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="price" class="form-control" 
                       autocomplete="off"  value="<?php echo $row['pap'] ?>"  required="required"  />
                     </div>
                   </div>
                   
                   
                   
                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "default Number" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="def" class="form-control" 
                       autocomplete="off"   value="<?php echo $row['max'] ?>" required="required"  />
                     </div>
                   </div>



  <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "Duration" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="Duration" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>



                
                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "annual increase" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="increase" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>




                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo "Insurance" ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="Insurance" class="form-control" 
                       autocomplete="off"  required="required"  />
                     </div>
                   </div>


                   <div class="form-group form-group-lg">

                     <div class="col-sm-10  col-md-6">

<label class="switch" style="margin-left: 194px;">
  <input type="checkbox" checked>
  <span class="slider round"></span>
</label>
</div>
</div>



    
                     <div class="form-group">
                     <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="Save" class="btn btn-primary  btn-lg" >
                     </div>
                   </div>


              </form>
       
            </div>

 <?php 
   /* if of end of edit page */
             }else{
              echo '<div class="container">';
              $mes='<div class="alert alert-danger">You are not alwoed to come here </div>';
              Redurict($mes,'');
              echo '</div>';

            }

       }
//*******************************************END if of Edit Member Page******************************//
//////////////////////////////////////////////////////////////////////////////////////////////////////////




         
  elseif($do=='Update'){ /**********start if  of update page********************************************/
         echo '<h1 class="text-center">Update machines Information </h1>';
         echo '<div class="container">';

         if( $_SERVER['REQUEST_METHOD']=='POST'){///start if of check if it post ****/

               $cid   =$_POST['cid'];
               $mid   = $_POST['code'];
               $date  =$_POST['date'];
               $varid =$_POST['userid'];


               //chech if the passwored is empety or not


                   $formErrors=array();// var of array to have the error
                

                  foreach ($formErrors as  $error) {
                    echo $error ;
                  }


          if(empty($formErrors)){ /**start if only to complet to  updet if no error*////


           $stmt=$con->prepare("UPDATE machin  SET  mid=?, cid=? ,datead=?  WHERE id=? ");


           $stmt->execute(array($mid,$cid,$date,$varid));
           $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Record Updated </div>';
           Redurict($mas,'back');

          }/********end  if only to complet to  updet if no error*/

            }///end if of check if it post ****/
            else{
              echo '<div class="container">';
              $mas='<div class=" alert alert-danger"> YOU Not alowed to came here </div>';
              Redurict($mas,'back');
              echo '</div>';
            }
echo '</div>';
   }/**********end if of updet page********************************/
////////////////////////////////////////////////////////////////////






elseif($do=='Delete'){ /**********start if  of Delet page********************************************/

echo '<div class="container">';
echo '<h1 class="text-center"> Delete page </h1>';

$id=isset($_GET['id']) && is_numeric($_GET['id'])? intval($_GET['id']):0;

 
                  $stmt=$con->prepare("DELETE FROM machin WHERE id=?");
                  $stmt->execute(array($id));
                  if($stmt->rowCount() > 0){

                   $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Recored deleted'.'</div>';
                   Redurict($mas,'back');
                   }
          

echo '</div>';
}/**********end if of Delete page********************************************************************/
////////////////





//////////////////////////////////////////////////////////////////////////////////////////////////////////
        include 'footer.php';
 //end of if check if is set session username camed or not****************************///////////////////////
    }else{
      
      echo 'You are not allawed to come here ';
      
    
    }