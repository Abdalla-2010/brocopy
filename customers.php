<?php 
  ob_start();
    session_start();
    if(isset($_SESSION['username'])){//  start of if check if is set session username camed or not
        $pagetitle="Customers";
        include 'init.php';
          
        $do=isset($_GET['do'])? $_GET['do']:'Manage'; //check if do==what ?  ****************************
//////////////////////////////////////////////////////////////////////

//*****************start if of Manage Member Page***************//
        if($do=='Manage'){

  $stmt=$con->prepare("SELECT * FROM users WHERE GroupID =2  ");
        $stmt->execute();
        $row=$stmt->fetchAll();

         ?>
            <h1 class="text-center">Manage Customers </h1>
            <div class="container">
                          <a href="?do=Add"  class="btn  btn-primary"> <i class="fa  fa-plus "></i> New Member  </a>
              <div class="table-responsive">
                  <table class=" min-table text-center table table-bordered ">
                     <tr>
                        <td>#ID</td>
                        <td>UserName</td>
                        <td>Phone</td>
                       
                       
                        <td>Email</td>
                         <td>Type</td>
                          <td>Date</td>
                        
                        <td>Control</td>
                     </tr>




<?php   
foreach ($row as $k) {
  echo '<tr>';
      echo '<td>'.$k['ID'].'</td>';
      echo '<td>'.$k['name'].'</td>';
      echo '<td>'. $k['phone'].'</td>';
      
            echo '<td>'. $k['email'].'</td>';
            echo '<td>'. $k['type'].'</td>';            
            echo '<td>'. $k['pdate'].'</td>';      
      echo  "<td>
<a href='?do=Edit&userid=".$k['ID']."'
class='btn btn-success'><i class='fa fa-edit'></i> Edit </a>

<a href='?do=Delete&userid=".$k['ID']."'
class='btn btn-danger  comfirm'><i class='fa fa-close'></i>Delete</a>";


echo '</td>';

  echo '</tr>';
}

?>

                  </table>

              </div>


           </div>
       <?php  }//***********************************END if of Manage Member Page*********************//
////////////////////////////////////////////////////////////////////////////////////////////////////////// 





 //*************start if of Edit Member Page***********************// 
    elseif($do=='Add'){ /* if of start of edit page */
       ?> 
            
            <h1 class="text-center">Add Information </h1>
            <div class="container">
              <form class="form-horizontal"  action="?do=insert" method="POST">


                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo lang("USER_NAME") ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="username" class="form-control" 
                       autocomplete="off"    />
                     </div>
                   </div>

                     <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label"> Passwors</label>
                     <div class="col-sm-10 col-md-6">
                      <input type="Passwors" name="Password" class="form-control" 
                      autocomplete="new-password"  placeholder=" Enter The password " required="required">
                     </div>
                   </div>

                     <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Phone</label>
                     <div class="col-sm-10 col-md-6">
                      <input type="text" name="phone"  maxlength="11"
                       class="form-control"  required="required" >
                     </div>
                   </div>


                <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Email</label>
                     <div class="col-sm-10 col-md-6">
                      <input type="text" name="Email"
                       class="form-control"  >
                     </div>
                   </div>


                     <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Notes</label>
                     <div class="col-sm-10 col-md-6">
                      <input type="text" name="notes"
                       class="form-control"   >
                     </div>
                   </div>


   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Date</label>
                     <div class="col-sm-10 col-md-6">
                      <input type="date" name="pdate"
                       class="form-control"  >
                     </div>
                   </div>






<div class="form-group form-group-lg">
                         <label class="col-sm-2  control-label">Type</label>
                         <div class="col-sm-10  col-md-6">
                          <select class="form-control" name="type">
                          <option value="Normal">Normal</option>
                          <option value="Maintenance contract">Maintenance contract</option>
                          <option value="Rental"> Rental</option>
                          <option value="Mixpix Customer">Mixpix Customer</option>
                        
                            </select>
                          </div>
                        </div>






                     <div class="form-group">
                     <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="Save" class="btn btn-primary  btn-lg" >
                     </div>
                   </div>

              </form>
            </div>

 <?php 
   /* if of end of edit page */
          

       }
//*******************************************END if of Edit Member Page******************************//
//////////////////////////////////////////////////////////////////////////////////////////////////////////




         
  elseif($do=='insert'){ /**********start if  of update page********************************************/
         echo '<h1 class="text-center">Insert Member </h1>';
         echo '<div class="container">';

         if( $_SERVER['REQUEST_METHOD']=='POST'){///start if of check if it post ****/

               $varname = $_POST['username'];
               $varemail=$_POST['Email'];
               $pass= $_POST['Password'];
               $phone=$_POST['phone'];
               $type=$_POST['type'];
               $notes=$_POST['notes'];
               $pdate=$_POST['pdate'];
               $hashdpass=sha1($pass);
               $gr=2;




                   $formErrors=array();// var of array to have the error

                    if(strlen($varname) < 3){ //  start if stmt only**//
                     $formErrors[]='<div class="alert alert-danger">usermname cant be <strong>less than 4 char</strong></div>';
                  } //  end if stmt only**//


                    
                  if(empty($varname)){ //  start if stmt only**//
                  $formErrors[]='<div class="alert alert-danger">usermname cant be<strong> empety</strong> </div>';
                  } // end if stmt only**//

                  foreach ($formErrors as  $error) {
                    echo $error ;
                  }

                 if(checkItem('name','users',$varname)>=1){ //  start if stmt only**//
                     $formErrors[]='<div class="alert alert-danger">usermname cant be <strong>Repeated</strong></div>';
                  } //  end if stmt only**//


          if(empty($formErrors)){ /**start if only to complet to  updet if no error*////

  $stmt=$con->prepare(" INSERT INTO 
                      users (name, Password,email , phone,GroupID,nots,st,pdate,type) 
                     VALUES (:zuser , :zpass , :zmail,:zphone , :zgroup,:znots,:zst,:zd,:zx)");

        $stmt->execute(array(

                          'zuser'=>$varname,
                          'zpass'=>$hashdpass,
                          'zmail'=>$varemail,
                          'zgroup'  =>$gr,
                          'zphone'=>$phone,
                          'znots'=>$notes,
                          'zst'=>0,
                          'zx'=>$type,
                          'zd'=>$pdate

                          
                          
                           ));



           $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Record Inserted </div>';
           echo 
           Redurict($mas,'back');

          }/********end  if only to complet to  updet if no error*/

else{

  print_r($formErrors);
}



            }///end if of check if it post ****/
            else{
              echo '<div class="container">';
              $mas='<div class=" alert alert-danger"> YOU Not alowed to came here </div>';
              Redurict($mas,'back');
              echo '</div>';
            }
echo '</div>';
   }/**********end if of updet page********************************/
////////////////////////////////////////////////////////////////////



       




 //*************start if of Edit Member Page***********************// 
    elseif($do=='Edit'){ /* if of start of edit page */
           //echo $_SESSION['ID'];
           $userid=isset($_GET['userid']) && is_numeric($_GET['userid'])? intval($_GET['userid']):0;
           $stmt=$con->prepare("SELECT * 
                                FROM 
                                    users 
                                WHERE 
                                    ID=? 
                                   
                                
                              LIMIT 1");
           $stmt->execute(array($userid));
           $row=$stmt->fetch();

            $count=$stmt->rowCount();

            if($stmt->rowCount() > 0){  ?> 
            
            <h1 class="text-center">Edit Information </h1>
            <div class="container">
              <form class="form-horizontal"  action="?do=Update" method="POST">
                      <input type="hidden" name="userid" value="<?php echo $userid ?>" />

                   <div class="form-group form-group-lg">

                     <label class="col-sm-2  control-label"><?php echo lang("USER_NAME") ?></label>
                     <div class="col-sm-10  col-md-6">
                      <input type="text" name="username" class="form-control" 
                      value="<?php echo $row['name'] ?>" autocomplete="off"  required="required"  />
                     </div>
                   </div>

                     <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label"> Passwors</label>
                     <div class="col-sm-10 col-md-6">
                      <input type="hidden" name="oldPassword" value="<?php  echo $row['Password'] ?>" >
                      <input type="Passwors" name="newPassword" class="form-control"  autocomplete="new-password"  placeholder=" Leave Blank If You Dont To Change ">
                     </div>
                   </div>

                     <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Phone </label>
                     <div class="col-sm-10 col-md-6">
                      <input type="text" name="phone"
                       value="<?php echo $row['phone'] ?>" class="form-control"  required="required" >
                     </div>
                   </div>


    <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Email </label>
                     <div class="col-sm-10 col-md-6">
                      <input type="text" name="Email"
                       value="<?php echo $row['email'] ?>" class="form-control"  required="required" >
                     </div>
                   </div>


    <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Notes </label>
                     <div class="col-sm-10 col-md-6">
                      <input type="text" name="notes"
                       value="<?php echo $row['nots'] ?>" class="form-control"   >
                     </div>
                   </div>



    <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Date </label>
                     <div class="col-sm-10 col-md-6">
                      <input type="date" name="pdate"
                       value="<?php echo $row['pdate'] ?>" class="form-control"   >
                     </div>
                   </div>


  
                   <div class="form-group form-group-lg">
                         <label class="col-sm-2  control-label">Type</label>
                         <div class="col-sm-10  col-md-6">
                          <select class="form-control" name="type">
                         <?php


           
?>
<option value="Male" <?php if($row["type"] == 'Normal') echo"selected"; ?> > Normal </option>
<option value="Female" <?php if($row["type"] == 'Maintenance contract') echo"selected"; ?> > Maintenance contract</option>
<option value="Company" <?php if($row["type"] == 'Rental') echo"selected"; ?> > Rental</option>
<option value="Company" <?php if($row["type"] == 'Mixpix Customer') echo"selected"; ?> > Mixpix Customer</option>

                            </select>
                          </div>
                        </div>







    
                     <div class="form-group">
                     <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="Save" class="btn btn-primary  btn-lg" >
                     </div>
                   </div>

              </form>
            </div>

 <?php 
   /* if of end of edit page */
             }else{
              echo '<div class="container">';
              $mes='<div class="alert alert-danger">You are not alwoed to come here </div>';
              Redurict($mes,'');
              echo '</div>';

            }

       }
//*******************************************END if of Edit Member Page******************************//
//////////////////////////////////////////////////////////////////////////////////////////////////////////




         
  elseif($do=='Update'){ /**********start if  of update page********************************************/
         echo '<h1 class="text-center">Update Member </h1>';
         echo '<div class="container">';

         if( $_SERVER['REQUEST_METHOD']=='POST'){///start if of check if it post ****/

               $varid =$_POST['userid'];
               $varname = $_POST['username'];
               $varemail=$_POST['Email'];
               $phone=$_POST['phone'];
               $type=$_POST['type'];
               $notes=$_POST['nots'];
               $pdate=$_POST['pdate'];              



               $titel=2;

               //chech if the passwored is empety or not
               $varpas=empty($_POST['newPassword'])? $_POST['oldPassword']:sha1($_POST['newPassword']) ;

                   $formErrors=array();// var of array to have the error

                    if(strlen($varname) < 3){ //  start if stmt only**//
                     $formErrors[]='<div class="alert alert-danger">usermname cant be <strong>less than 4 char</strong></div>';
                  } //  end if stmt only**//


                   if(strlen($varname) >20 ){ //  start if stmt only**//
                     $formErrors[]='<div class="alert alert-danger">usermname cant be <strong>>more than 20 char</strong></div>';
                  }//  end if stmt only**//

                    
                  if(empty($varname)){ //  start if stmt only**//
                  $formErrors[]='<div class="alert alert-danger">usermname cant be<strong> empety</strong> </div>';
                  } // end if stmt only**//


                

                  foreach ($formErrors as  $error) {
                    echo $error ;
                  }


          if(empty($formErrors)){ /**start if only to complet to  updet if no error*////


           $stmt=$con->prepare("UPDATE users  SET  name=?, phone=? ,Password=? ,email=?,nots=?,type=?,pdate=? WHERE ID=? ");

           $stmt->execute(array($varname,$phone,$varpas,$varemail,$notes,$type,$pdate,$varid));
           $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Record Updated </div>';
           Redurict($mas,'back');

          }/********end  if only to complet to  updet if no error*/

            }///end if of check if it post ****/
            else{
              echo '<div class="container">';
              $mas='<div class=" alert alert-danger"> YOU Not alowed to came here </div>';
              Redurict($mas,'back');
              echo '</div>';
            }
echo '</div>';
   }/**********end if of updet page********************************/
////////////////////////////////////////////////////////////////////






elseif($do=='Delete'){ /**********start if  of Delet page********************************************/

echo '<div class="container">';
echo '<h1 class="text-center"> Delete page </h1>';

$userid=isset($_GET['userid']) && is_numeric($_GET['userid'])? intval($_GET['userid']):0;

    $checkItem=checkItem('ID','users',$userid);

          if($checkItem > 0){  
                  $stmt=$con->prepare("DELETE FROM users WHERE ID=?");
                  $stmt->execute(array($userid));
                  if($stmt->rowCount() > 0){

                   $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Recored deleted'.'</div>';
                   Redurict($mas,'back');
                   }
          }

echo '</div>';
}/**********end if of Delete page********************************************************************/
////////////////





//////////////////////////////////////////////////////////////////////////////////////////////////////////
        include 'footer.php';
 //end of if check if is set session username camed or not****************************///////////////////////
    }else{
      
      echo 'You are not allawed to come here ';
      
    
    }