<?php 
ob_start();

    session_start();
    if(isset($_SESSION['username'])){//  start of if check if is set session username camed or not
        $pagetitle="";
        include 'init.php';

  
        $do=isset($_GET['do'])? $_GET['do']:'Manage'; //check if do==what ?****
/////////////////////////////////////////////////////////////////

if($do=='Manage'){


        $stmt=$con->prepare("SELECT
                                 *
                           FROM 
                               items
                          
                             ");

        $stmt->execute();
        $items=$stmt->fetchAll();

  
         ?>
            <h1 class="text-center">Product Management </h1>
            <div class="container">
              <div>
                  <div class="form-group">
                      <input type="search" class="form-control search-products" name="search-products" placeholder="Search products by name" />
                      <!-- <span class="fa fa-search search-icon"></span> -->
                  </div>
              </div>
                            <a href="?do=Add"  class="btn  btn-primary"> <i class="fa  fa-plus "></i> New Item  </a>
              <div class="table-responsive">
                  <table class=" min-table text-center table table-bordered ">
                     <tr>

                        <td>Name</td>
                        <td>Quantity</td>
                        <td>Actual price</td>
                        <td>Sales price</td>
                        <td>SKU</td>
                        <td style="width: 100px;">Descrption</td>
                        <td>Img</td>
                        <td>Control</td>
                     </tr>


<?php   
foreach ($items as $item) {
  echo '<tr class="product-row" data-name="'.$item['Name'].'">';

      echo '<td>' .$item['Name'].'</td>';
if($item['pro_quantity'] <=5){

  echo '<td style="background:red;">' .$item['pro_quantity'].'</td>';
}else{

      echo '<td>' .$item['pro_quantity'].'</td>';

}

      echo '<td>' .$item['mainprice'].'</td>';
      echo '<td>' .$item['proPrice'].'</td>';
      echo '<td>' .$item['code'].'</td>';



      echo '<td>' .$item['Descrption'].'</td>';



echo '<td><img  width="100" height="100" src="upload/'.$item['img'].'"></td>';


     
  echo "<td>
<a href='items.php?do=Edit&itemid=".$item['Item_ID']."'
class='btn btn-success'><i class='fa fa-edit'></i> Edit </a>

<a href='items.php?do=Delete&itemid=".$item['Item_ID']."'
class='btn btn-danger  comfirm'><i class='fa fa-close'></i>Delete</a>";



echo '</td>';

  echo '</tr>';
}

?>

                  </table>

              </div>

           </div>
       <?php

}
/////////////////////////////////////////////////////////////////////////////////////

elseif($do=='Add'){

?>


            <h1 class="text-center">Add New Item </h1>
            <div class="container">
              <form class="form-horizontal"  action="?do=Insert" method="POST"  enctype="multipart/form-data">

                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Name</label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="name" 
                      class="form-control"  
                      placeholder="Name Of Item" 
                      required="required"  />
                     </div>
                   </div>


                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Descrption</label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="descrption" 
                      class="form-control"  
                      placeholder="Descrption Of Item" 
                       />
                     </div>
                   </div>



           

                       <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">SKU</label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="code" 
                      class="form-control"  
                      placeholder="Code Of Item" 
                       />
                     </div>
                   </div>




                       <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Actual price </label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="APrice" 
                      class="form-control"  
                      placeholder="Actual price Of Item" 
                       required="required" />
                     </div>
                   </div>



                   


                       <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Sales price </label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="Price" 
                      class="form-control"  
                      placeholder="Sales price Of Item" 
                       required="required" />
                     </div>
                   </div>


                       <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Quantity</label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="qty" 
                      class="form-control"  
                      placeholder="Quantity Of Item" 
                      />
                     </div>
                   </div>



                      <div class="form-group form-group-lg ">
       <div id="gellery"></div><div style="clear:both;"></div><br/>
            <label class="col-sm-2  control-label">Image</label>
            <div class="col-sm-10 col-md-6">
            <input 
               type="file" 
               name="avter" 
               class="form-control" 
               multiple
                >
            </div>
          </div>


                     <div class="form-group">
                     <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="Add Item " class="btn btn-primary  btn-lg" >
                     </div>
                   </div>

              </form>
            </div>

<?php

}

///////////////////////////////////////////////////////////////////////////////////////////////
elseif($do == 'Insert'){

  if($_SERVER['REQUEST_METHOD']=='POST'){
          echo '<h1 class="text-center"> Insert Item</h1>';
          echo '<div class="container">';       
         $itemname      =$_POST['name'];
         $descrption    =$_POST['descrption'];
         $code          =$_POST['code'];
         $qty           =$_POST['qty'];
         $Price         =$_POST['Price'];
         $APrice         =$_POST['APrice'];



         $formErrors=array();



         if(empty($itemname )){
            $formErrors[]='Name Of  Item Cant Be <strong>Empty</strong>';
         }

         if(empty($descrption)){
            $formErrors[]='Descrption Of Item Cant Be <strong>Empty</strong>';
         }

        



            
      

         foreach ($formErrors as $error) {
               echo '<div class="alert alert-danger">'.$error.'</div>';
         }



if(empty($formErrors)){
    


            $avaterName= $_FILES['avter']['name'];
            $avaterType= $_FILES['avter']['type'];
            $avaterTmp = $_FILES['avter']['tmp_name'];
            $avatersize= $_FILES['avter']['size'];
$avater=rand().'_'.$avaterName;
move_uploaded_file($avaterTmp,"upload/".$avater);



  $stmt=$con->prepare(" INSERT INTO 
  items (Name,Descrption,pro_quantity,proPrice,code,img,mainprice) 
  VALUES(:zname , :zdic ,:zqty,:zprice,:zcode,:zimg,:zmainprice)");

        $stmt->execute(array(
                          'zname'  =>$itemname,
                          'zdic'   =>$descrption,
                          'zqty'   =>$qty,
                          'zprice' =>$Price,
                          'zcode'  =>$code,
                          'zimg'   =>$avater,
                          'zmainprice'=>$APrice

                           ));
     $mes='<div class="alert  alert-success">'.$stmt->rowCount(). '  Recored Inserted'.'</div>';
     Redurict($mes,'back');


}



      echo '</div>';

      }else{

         $mas='<div class="alert alert-danger">You are not alwoed to come here</div>';
         Redurict($mas,'h');
             
      }


}

//////////////////////////////////////////////////////////////////////////////////////
elseif($do=='Edit'){ 

 $id=isset($_GET['itemid']) && is_numeric($_GET['itemid'])? intval($_GET['itemid']):0;
           $stmt=$con->prepare("SELECT *  FROM  items  WHERE  Item_ID=? ");
           $stmt->execute(array($id));
           $it=$stmt->fetch();
        
          


         
           $count=$stmt->rowCount();

            if($stmt->rowCount() > 0){  ?> 
            
             <h1 class="text-center">Edit Item </h1>
            <div class="container">
              <form class="form-horizontal"  action="?do=Update" method="POST">
                      <input type="hidden" name="itemid" value="<?php echo $id ?>" />

                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Name</label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="name" 
                      class="form-control"  
                      placeholder="Name Of Item" 
                      value="<?php echo $it['Name'] ?>"
                      required="required"  />
                     </div>
                   </div>


                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Descrption</label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="descrption" 
                      class="form-control"  
                      placeholder="Descrption Of Item" 
                      value="<?php  echo $it['Descrption']; ?>"
                       required="required" />
                     </div>
                   </div>



                       <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">SUK</label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="code" 
                      class="form-control"  
                      value="<?php  echo $it['code']; ?>"
                       required="required" />
                     </div>
                   </div>


    <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Actual price </label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="APrice" 
                      class="form-control"  
                      value="<?php  echo $it['mainprice']; ?>"                       
                      required="required" />
                     </div>
                   </div>



                   


             
                       <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Sales price </label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="Price" 
                      class="form-control"  
                      value="<?php  echo $it['proPrice']; ?>"
                       required="required" />
                     </div>
                   </div>



                   

                       <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Quantity </label>
                     <div class="col-sm-10  col-md-6">
                      <input 
                      type="text" 
                      name="qty" 
                      class="form-control"  
                      value="<?php  echo $it['pro_quantity']; ?>"
                       required="required" />
                     </div>
                   </div>


                   


                      

                     

                     <div class="form-group">
                     <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="Save" class="btn btn-primary  btn-lg" >
                     </div>
                   </div>

              </form>
            </div>

 <?php 
   /* if of end of edit page */
             }else{
              echo '<div class="container">';
              $mes='<div class="alert alert-danger">You are not alwoed to come here </div>';
              Redurict($mes,'');
              echo '</div>';

            }
}
/////////////////////////////////////////////////////////////////////////////////////
         
elseif($do=='Update'){

echo '<h1 class="text-center">Update Item </h1>';
         echo '<div class="container">';
 
         if( $_SERVER['REQUEST_METHOD']=='POST'){///start if of check if it post *****************/

               $varid     =$_POST['itemid'];
               $varname   =$_POST['name'];
               $vardec    =$_POST['descrption'];
               $price     =$_POST['Price'];
               $qty       =$_POST['qty'];
               $APrice    =$_POST['APrice'];
               $SKU       =$_POST['code'];

         


            

                
              $formErrors=array();



         if(empty($varname )){
            $formErrors[]='Name Of  Item Cant Be <strong>Empty</strong>';
         }

         if(empty($vardec)){
            $formErrors[]='Descrption Of Item Cant Be <strong>Empty</strong>';
         }

      
        
         foreach ($formErrors as $error) {
               echo '<div class="alert alert-danger">'.$error.'</div>';
         }
          if(empty($formErrors)){ /*************start if only to complet to  updet if no error*////
          $stmt=$con->prepare("UPDATE 
                                    items  
                               SET  
                                   Name=?,
                                   Descrption=?,
                                   pro_quantity=?,
                                   proPrice=?,
                                   code=?,
                                   mainprice=?
                               
                              WHERE Item_ID=? ");

$stmt->execute(array($varname,$vardec,$qty,$price,$SKU,$APrice,$varid));
           $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Record Updated </div>';
           Redurict($mas,'back');
          }/*************end  if only to complet to  updet if no error*////
          

            }///end if of check if it post *****************/
            else{
              echo '<div class="container">';
              $mas='<div class=" alert alert-danger"> YOU Not alowed to came here </div>';
              Redurict($mas,'back');
              echo '</div>';
            }


echo '</div>';
  

}
//////////////////////////////////////////////////////////////////////////////////////////

elseif($do=='Delete'){ 


echo '<div class="container">';
echo '<h1 class="text-center"> Delete Item</h1>';

$id=isset($_GET['itemid']) && is_numeric($_GET['itemid'])? intval($_GET['itemid']):0;

    $checkItem=checkItem('Item_ID','items',$id);

          if($checkItem > 0){  
                  $stmt=$con->prepare("DELETE FROM items WHERE Item_ID=?");
                  $stmt->execute(array($id));
                  if($stmt->rowCount() > 0){

                   $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Recored deleted'.'</div>';
                   Redurict($mas,'back');
                   }
          }else{

echo 'This ID is not Exist';

          }

echo '</div>';
}
//////////////////////////////////////////////////////////////////////
        include 'footer.php';
 //end of if check if is set session username camed or not****************************///////////////////////
    }else{
      
      echo 'You are not allawed to come here ';
      
    
    }
    
    ?>
    <script src="js/main.js"></script>